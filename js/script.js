$(document).ready(function () {
  // красивый select
  if ($(".js-select").length) {
    $(".js-select").select2({ minimumResultsForSearch: Infinity });
  }
  // /красивый select
  // плавный скролл к якорю
  $(".js-scroll").click(function () {
    $("html").animate(
      {
        scrollTop: $($.attr(this, "href")).offset().top,
      },
      300
    );
    return false;
  });
  // плавный скролл к якорю
  // маска для инаупов
  if ($("[data-inputmask]").length) {
    $(":input").inputmask();
  }
  // /маска для инпупов
  // меню
  $(".js-menu-show").on("click", function () {
    $(".js-menu-content").slideDown();
  });
  $(".js-menu-hide").on("click", function () {
    $(".js-menu-content").slideUp();
  });
  // /меню
  // video
  $(".js-video-container").on("click", function () {
    if ($(".js-video-container").hasClass("play")) {
      $(".js-video").get(0).pause();
      $(".js-video-container").removeClass("play");
    } else {
      $(".js-video").get(0).play();
      $(".js-video-container").addClass("play");
    }
    $(".js-play").fadeToggle()();
  });
  // /video
  // Доступная стоимость ветеринарных услуг
  $(".js-servise-li").on("click", function () {
    let ids = $(this).attr("data-id");
    let elem = $("#" + ids);
    $(".js-servise-li.active").removeClass("active");
    $(".js-servise-content.active").fadeOut("active");
    $(".js-servise-content.active").removeClass("active");
    elem.addClass("active");
    $(this).addClass("active");
    elem.fadeIn();
    $(".js-servise-toggle").text($(this).text());
    if ($(window).width() < 768) {
      $(".js-servise").toggleClass("open");
      $(".js-servise-list").slideToggle();
    }
  });
  $(".js-servise-toggle").on("click", function () {
    $(".js-servise").toggleClass("open");
    $(".js-servise-list").slideToggle();
  });
  // /Доступная стоимость ветеринарных услуг
  //Наши специалисты
  if ($(".js-team").length) {
    var swiper = new Swiper(".js-team", {
      slidesPerView: 3,
      slidesPerColumn: 2,
      slidesPerColumnFill: "row",
      slidesPerGroup: 6,
      spaceBetween: 30,
      pagination: {
        el: ".js-team-pagination",
        clickable: true,
      },
      navigation: {
        nextEl: ".js-team-next",
        prevEl: ".js-team-prev",
      },
      breakpoints: {
        300: {
          slidesPerView: 1,
          slidesPerColumn: 1,
          slidesPerColumnFill: "row",
          slidesPerGroup: 1,
          spaceBetween: 15,
        },
        767: {
          slidesPerView: 1,
          slidesPerColumn: 1,
          slidesPerColumnFill: "row",
          slidesPerGroup: 1,
          spaceBetween: 15,
        },
        1024: {
          slidesPerView: 2,
          slidesPerColumn: 2,
          slidesPerColumnFill: "row",
          slidesPerGroup: 4,
          spaceBetween: 30,
        },
        1200: {
          slidesPerView: 3,
          slidesPerColumn: 2,
          slidesPerColumnFill: "row",
          slidesPerGroup: 6,
          spaceBetween: 30,
        },
      },
    });
  }
  // /Наши специалисты
  //Фото нашей клиники
  if ($(".js-photos").length) {
    var swiper = new Swiper(".js-photos", {
      slidesPerView: 3,
      slidesPerColumn: 2,
      slidesPerColumnFill: "row",
      slidesPerGroup: 6,
      spaceBetween: 30,
      pagination: {
        el: ".js-photos-pagination",
        clickable: true,
      },
      navigation: {
        nextEl: ".js-photos-next",
        prevEl: ".js-photos-prev",
      },
      breakpoints: {
        300: {
          slidesPerView: 1,
          slidesPerColumn: 1,
          slidesPerColumnFill: "row",
          slidesPerGroup: 1,
          spaceBetween: 15,
        },
        600: {
          slidesPerView: 1,
          slidesPerColumn: 1,
          slidesPerColumnFill: "row",
          slidesPerGroup: 1,
          spaceBetween: 15,
        },
        1024: {
          slidesPerView: 2,
          slidesPerColumn: 2,
          slidesPerColumnFill: "row",
          slidesPerGroup: 4,
          spaceBetween: 30,
        },
        1200: {
          slidesPerView: 3,
          slidesPerColumn: 2,
          slidesPerColumnFill: "row",
          slidesPerGroup: 6,
          spaceBetween: 30,
        },
      },
    });
  }
  // /Фото нашей клиники

  //Отзывы клиентов
  if ($(".js-reviews").length) {
    var swiper = new Swiper(".js-reviews", {
      slidesPerView: 2,
      spaceBetween: 30,
      pagination: {
        el: ".js-reviews-pagination",
        clickable: true,
      },
      navigation: {
        nextEl: ".js-reviews-next",
        prevEl: ".js-reviews-prev",
      },
      breakpoints: {
        768: {
          slidesPerView: 1,
          spaceBetween: 10,
        },
      },
    });
  }
  // /Отзывы клиентов
  // Готовимся к визиту в вашу клинику
  $(".js-visit-ask").on("click", function () {
    $(this).parents(".js-visit-item").find(".js-visit-answer").slideToggle();

    $(".js-visit-item.open").find(".js-visit-answer").slideUp();
    $(".js-visit-item.open").toggleClass("open");

    $(this).parents(".js-visit-item").toggleClass("open");
  });
  // Готовимся к визиту в вашу клинику

  // Карта
  ymaps.ready(function () {
    var myMap = new ymaps.Map(
        "map",
        {
          center: [54.761132, 32.102188],
          zoom: 16,
          controls: [],
        },
        {
          searchControlProvider: "",
        }
      ),
      // Создаём макет содержимого.
      MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
        '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
      ),
      myPlacemarkWithContent = new ymaps.Placemark(
        [54.759791, 32.104027],
        {},
        {}
      );

    myMap.geoObjects.add(myPlacemarkWithContent);
  });
  // /Карта
  // Оборудование с которым работаем
  $(".js-equipment-li").on("click", function () {
    $(".js-equipment .blue").removeClass("blue");
    $(this).addClass("blue");
    let pictureId = $(this).attr("data-id");
    $(".js-equipment-img .active").removeClass("active");
    $("#" + pictureId).addClass("active");
  });
  // /Оборудование с которым работаем
});
